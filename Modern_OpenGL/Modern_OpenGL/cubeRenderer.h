#pragma once

namespace cubeRenderer//maybe a static method only class would really be better since vao, vbo and ebo are not supposed to be accessed from outside
{
	void init();

	void renderCube();
	
	void destroy();

	extern unsigned int vao, vbo, ebo, num_indicies;//use extern keyword for global variables to prevent linking errors and init variables in src
}//don't use static in namespaces as it means that every copy of the namespace will have it's own version