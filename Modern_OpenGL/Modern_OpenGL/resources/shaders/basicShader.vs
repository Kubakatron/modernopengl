#version 330 core
layout (location = 0) in vec3 aPos;			//layout (location=x) specifies var location in vertex attribs, in type name; declares an input variable
layout (location = 1) in vec2 aTexCoord;

out vec2 texCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);		//matrix multiplication is read from right to left
	texCoord = aTexCoord;
}