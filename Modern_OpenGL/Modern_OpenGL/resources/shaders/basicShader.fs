#version 330 core
out vec4 FragColor;						//declares an output variable

in vec2 texCoord;

uniform vec4 customColor;				//if you declare a uniform variable and not use it, it will get silenly removed by the compiler

uniform sampler2D primaryTexture;		//should automaticly assign the currently bound texture if only using one texture unit
uniform sampler2D secondaryTexture;		//if using more than one texture unit, should be set manualy

uniform float mixValue;

void main()
{
	vec4 primaryColor = texture(primaryTexture, texCoord);
	vec4 secondaryColor = texture(secondaryTexture, texCoord);
	FragColor = mix(primaryColor, secondaryColor, mixValue * secondaryColor.r) * customColor;
	//FragColor = texture(boundTexture, texCoord) * vec4(0, texCoord, 1);
}