#pragma once
#include <fstream>

class Shader
{
public:
	Shader(const string& fileName);
	~Shader();
	void use();
	void setVec4f(const string& name, float r, float g, float b, float a);
	void setMat4f(const string& name, glm::mat4 mat);
	void setInt(const string& name, int value);
	void setFloat(const string& name, float value);

private:

	unsigned int shaderProgram;

	static unsigned int createShader(const string& source, GLenum type);
	static void checkError(unsigned int shader, unsigned int flag, bool isProgram, const string& errorMessage);
	static string loadFile(const string& fileName);
};