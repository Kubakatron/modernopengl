#pragma once

void processInput(GLFWwindow* window);

void onFramebufferSizeChange(GLFWwindow* window, int width, int height);

void onMouseMoved(GLFWwindow* window, double xPos, double yPos);

void onMouseScrolled(GLFWwindow* window, double xOffset, double yOffset);

int screenWidth, screenHeight;