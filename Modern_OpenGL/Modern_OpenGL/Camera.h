#pragma once
class Camera
{
public:
	enum Direction { Forward, Backward, Left, Right, Up, Down };

	Camera();
	~Camera();
	glm::mat4 getProjectionMatrix(float aspectRatio);
	glm::mat4 getViewMatrix();
	void move(Direction dir, float speed);
	void mouseMove(float xPos, float yPos);
	glm::vec3 getFront();
	glm::vec3 getPosition();
	float getFov();
	void setFov(float fov);
private:
	glm::vec3 cameraPos, cameraFront, cameraUp;
	float yaw, pitch, fov, sensitivity, lastX, lastY;
	bool firstInput;
};

