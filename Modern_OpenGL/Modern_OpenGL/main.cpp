#include "stdafx.h"
#include "main.h"

#include "Display.h"
#include "Shader.h"
#include "Camera.h"
#include "cubeRenderer.h"
#include "Texture.h"
#include <cmath>
//#include <vector>
#include <deque>

Display* display;
Camera camera;

float deltaTime, lastFrame = 0.0f;

deque <glm::vec3> cubes;

int main()
{
	display = new Display(800, 600, "GLFW OpenGL 3.3 Minimum Core Profile Window");
	glfwSetFramebufferSizeCallback(display->getWindow(), onFramebufferSizeChange);//set resize callback
	glfwSetCursorPosCallback(display->getWindow(), onMouseMoved);//set mouse movement callback
	glfwSetScrollCallback(display->getWindow(), onMouseScrolled);//set mouse scroll callback
	display->setCursorCapture(true);

	Shader basicShader("resources/shaders/basicShader");

	cubeRenderer::init();

	cubes = {
		//glm::vec3(0, 0, 0),
		//glm::vec3(1, 0, 0),
		//glm::vec3(0, 1, 0),
		//glm::vec3(0, 0, 1),
		//glm::vec3(0, 1, 1),
		//glm::vec3(4, 0, 0),
		//glm::vec3(0, 3, 6),
		//glm::vec3(3, 1, 1),
		//glm::vec3(4, 2, 1),
		//glm::vec3(4, 4, 6),
		//glm::vec3(0, 1, 5),
		//glm::vec3(4, 0, 1),
		//glm::vec3(0, 3, 0),

		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 2.0f, 0.0f),
		glm::vec3(1.0f, 3.0f, 0.0f),
		glm::vec3(2.0f, 3.0f, 0.0f),
		glm::vec3(3.0f, 2.0f, 0.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec3(0.0f, -2.0f, 0.0f),
		glm::vec3(1.0f, -3.0f, 0.0f),
		glm::vec3(2.0f, -3.0f, 0.0f),
		glm::vec3(3.0f, -2.0f, 0.0f),
		glm::vec3(5.0f, 0.0f, 0.0f),
		glm::vec3(6.0f, 0.0f, 0.0f),
		glm::vec3(7.0f, 0.0f, 0.0f),
		glm::vec3(6.0f, 1.0f, 0.0f),
		glm::vec3(6.0f, -1.0f, 0.0f),
		glm::vec3(9.0f, 0.0f, 0.0f),
		glm::vec3(10.0f, 0.0f, 0.0f),
		glm::vec3(11.0f, 0.0f, 0.0f),
		glm::vec3(10.0f, 1.0f, 0.0f),
		glm::vec3(10.0f, -1.0f, 0.0f)
	};

	for (unsigned int i = 0; i < 20; i++)
	{
		for (unsigned int j = 0; j < 20; j++)
		{
			cubes.push_back(glm::vec3(i-10.0f, -10.0f, j-10.0f));
		}
	}

	Texture primaryTexture = Texture::loadTexture("resources/textures/logo.png");
	Texture secondaryTexture = Texture::loadTexture("resources/textures/shizz.png");
	
	display->setClearColor(0.8f, 0.5f, 0, 1);
	 
	int colorCycle = 0;
	int rotation = 0;

	basicShader.use();
	basicShader.setInt("primaryTexture", 0);//set texture unit uniform names
	basicShader.setInt("secondaryTexture", 1);
	basicShader.setFloat("mixValue", 0.7f);

	while (!display->shouldClose())//main loop
	{
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		processInput(display->getWindow());

		colorCycle++;
		colorCycle %= 628;
		float result = sinf(colorCycle/100.0f);
		rotation += 1;
		rotation %= 360;

		basicShader.setVec4f("customColor", 1, 1, 1 * result, 1);
		basicShader.setFloat("mixValue", result*3);
		basicShader.setMat4f("projection", camera.getProjectionMatrix((float)display->getWidth() / display->getHeight()));//cache this

		float radius = 10.0f;
		float camX = sin(glfwGetTime()) * radius;
		float camZ = cos(glfwGetTime()) * radius;

		basicShader.setMat4f("view", camera.getViewMatrix());

		for (unsigned int i = 0; i < cubes.size(); i++)
		{
			glm::mat4 model;
			model = glm::translate(model, glm::vec3(cubes[i].x, cubes[i].y, cubes[i].z));
			basicShader.setMat4f("model", model);

			primaryTexture.bind(0);
			secondaryTexture.bind(1);

			cubeRenderer::renderCube();
		}

		display->update();
	}

	cubeRenderer::destroy();
	delete display;

    return EXIT_SUCCESS;
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	float cameraSpeed = 3.0f * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		cameraSpeed *= 2;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.move(Camera::Forward, cameraSpeed);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.move(Camera::Backward, cameraSpeed);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.move(Camera::Left, cameraSpeed);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.move(Camera::Right, cameraSpeed);
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
		camera.move(Camera::Up, cameraSpeed);
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
		camera.move(Camera::Down, cameraSpeed);

	display->setWireFrameMode(glfwGetKey(window, GLFW_KEY_Q));

	if(glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		float gridSize = 1.0f;
		glm::vec3 cube = camera.getPosition() + camera.getFront() * 3.0f;

		if (glfwGetKey(window, GLFW_KEY_E) != GLFW_PRESS)
		{
			cube.x = floor(cube.x / gridSize) * gridSize;
			cube.y = floor(cube.y / gridSize) * gridSize;
			cube.z = floor(cube.z / gridSize) * gridSize;
		}
		if(cubes.front() != cube)
			cubes.push_front(cube);
		if (cubes.size() > 3000)
			cubes.pop_back();

		cout << "Cube count: " << cubes.size() << endl;
	}
}

void onFramebufferSizeChange(GLFWwindow* window, int width, int height)//resize callback
{
	cout << "Window resized to: " << width << "x" << height << endl;
	display->setSize(width, height);
}

void onMouseMoved(GLFWwindow* window, double xPos, double yPos)//mouse movement callback
{
	camera.mouseMove(xPos, yPos);
}

void onMouseScrolled(GLFWwindow* window, double xOffset, double yOffset)// mouse scroll callback
{
	float fov = camera.getFov();
	fov -= yOffset;
	if (fov > 120)
		fov = 120;
	else if (fov < 35)
		fov = 35;
	camera.setFov(fov);
}