#include "stdafx.h"
#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

unsigned int Texture::boundTexture[] = {};

Texture Texture::loadTexture(const string& file)
{
	stbi_set_flip_vertically_on_load(true);

	int width, height, colorChannels;
	unsigned char* data = stbi_load(file.c_str(), &width, &height, &colorChannels, 0);//(file, x, y, colorChannels, number of components per pixel)

	if (!data)
	{
		cerr << "Failed to load texture: " << file << endl;
		return Texture(0, 0, 0, 0);
	}

	unsigned int id;

	glGenTextures(1, &id);//(how many textures to generate, texture)
	glBindTexture(GL_TEXTURE_2D, id);//bind texture

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);//texture wrapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);//minification using mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);//magnification

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);//(target, manual mipmap level - usage: calling method muliple times with incemented value, texture format, sizex, sizey, legacy border width, image format, data format, data)
	glGenerateMipmap(GL_TEXTURE_2D);//auto-generates mipmaps

	stbi_image_free(data);

	return Texture(id, width, height, colorChannels);
}

Texture::Texture(unsigned int id, int width, int height, int colorChannels)
{
	this->id = id;
	this->width = width;
	this->height = height;
	this->colorChannels = colorChannels;
}

Texture::~Texture()
{
	//TODO dispose of texture data
}

void Texture::bind(unsigned int textureUnit)//had to be optimised since binding textures is slow
{
	if (textureUnit > TEXTURE_UNIT_NUM)
		return;
	if (boundTexture[textureUnit] == this->id)
		return;
	cout << "BINDING TEXTURE: " << this->id << " TO TEXTURE UNIT: " << textureUnit << endl;
	glActiveTexture(GL_TEXTURE0+textureUnit);
	glBindTexture(GL_TEXTURE_2D, this->id);
	boundTexture[textureUnit] = this->id;
}