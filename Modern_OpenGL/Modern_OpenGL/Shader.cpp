#include "stdafx.h"
#include "Shader.h"


Shader::Shader(const string& fileName)
{
	unsigned int vShader = createShader(loadFile(fileName + ".vs"), GL_VERTEX_SHADER);
	unsigned int fShader = createShader(loadFile(fileName + ".fs"), GL_FRAGMENT_SHADER);

	this->shaderProgram = glCreateProgram();	//create shader program id

	glAttachShader(this->shaderProgram, vShader);
	glAttachShader(this->shaderProgram, fShader);

	glLinkProgram(this->shaderProgram);
	checkError(this->shaderProgram, GL_LINK_STATUS, true, "Program " + fileName + " compilation failed: ");
	glValidateProgram(this->shaderProgram);
	checkError(this->shaderProgram, GL_VALIDATE_STATUS, true, "Program " + fileName + " validation failed: ");

	glDeleteShader(vShader);	//delete shaders once they're no longer needed
	glDeleteShader(fShader);
}


Shader::~Shader()
{
	glDeleteProgram(this->shaderProgram);
}

void Shader::use()
{
	glUseProgram(this->shaderProgram);
}

void Shader::setVec4f(const string& name, float r, float g, float b, float a)
{
	int uniformLocation = glGetUniformLocation(this->shaderProgram, name.c_str());
	glUniform4f(uniformLocation, r, g, b, a);	//binds uniform to currently used shader therefore shader must be used first
}

void Shader::setMat4f(const string& name, glm::mat4 mat)
{
	int uniformLocation = glGetUniformLocation(this->shaderProgram, name.c_str());
	glUniformMatrix4fv(uniformLocation, 1, GL_FALSE, glm::value_ptr(mat));	//(location, count, swap columns and rows, value)
}

void Shader::setInt(const string& name, int value)
{
	int uniformLocation = glGetUniformLocation(this->shaderProgram, name.c_str());
	glUniform1i(uniformLocation, value);
}

void Shader::setFloat(const string& name, float value)
{
	int uniformLocation = glGetUniformLocation(this->shaderProgram, name.c_str());
	glUniform1f(uniformLocation, value);
}

unsigned int Shader::createShader(const string& source, GLenum type)
{
	const char* shaderSource = source.c_str();	//store shader source as a c string

	unsigned int shader = glCreateShader(type);	//generate shader id

	glShaderSource(shader, 1, &shaderSource, nullptr);	//send shader source to shader (shader id, num of strings, shader source as c string reference, ???)
	glCompileShader(shader);	//compile the shader

	checkError(shader, GL_COMPILE_STATUS, false, "Shader compilation failed: ");	//check if compilation failed

	return shader;
}

void Shader::checkError(unsigned int shader, unsigned int flag, bool isProgram, const string& errorMessage)
{
	int success;
	char infoLog[512];

	if (isProgram)	//check if succeded
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (!success)
	{
		if (isProgram)	//get info log
			glGetProgramInfoLog(shader, sizeof(infoLog), nullptr, infoLog);
		else
			glGetShaderInfoLog(shader, sizeof(infoLog), nullptr, infoLog);

		cerr << errorMessage << infoLog << endl;
	}
}

string Shader::loadFile(const string& fileName)
{
	ifstream file;

	file.open(fileName.c_str());	//open file stream with c string fileName

	string output;
	string line;

	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + '\n');
		}
	}
	else
	{
		cerr << "Unable to load file \"" << fileName << "\"" << endl;
	}

	return output;
}