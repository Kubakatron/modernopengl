#include "stdafx.h"
#include "Display.h"
#include <functional>

Display::Display(unsigned int width, unsigned int height, const string title)
{
	glfwInit();//init glfw

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//set opengl 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);//set core profile (no fixed pipeline)
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);//needed for mac os x compatibility
	glfwWindowHint(GLFW_RESIZABLE, true);//disable resizing window

	this->window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

	glfwGetFramebufferSize(this->window, &this->screenWidth, &this->screenHeight);//actual value (for mac retina displays)

	if (this->window == nullptr)
	{
		glfwTerminate();
		cerr << "Failed to create window";
	}

	glfwMakeContextCurrent(this->window);

	glewExperimental = GL_TRUE;//not actually experimental, just named that way - enables 'modern' glew

	GLenum result = glewInit();//initialise glew
	if (result != GLEW_OK)
	{
		glfwTerminate();//destroys all windows, allocated resources and exits glfw
		cerr << "Failed to initialise GLEW: " << glewGetErrorString(result);
	}

	glViewport(0, 0, this->screenWidth, this->screenHeight);//set opengl viewport

	//glfwSetFramebufferSizeCallback(this->window, framebuffer_size_callback);//set resize callback function

	glEnable(GL_DEPTH_TEST);

	cout << "Created window " << width << "x" << height << " with title: " << title << endl;
}

Display::~Display()
{
	glfwTerminate();
}

bool Display::shouldClose()
{
	return glfwWindowShouldClose(this->window);
}

void Display::setClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void Display::setWireFrameMode(bool enabled)
{
	if (enabled)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);//set polygon rendering mode to wireframe(line) instead of fill
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Display::setCursorCapture(bool enabled)
{
	if (enabled)
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	else
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Display::update()
{
	glfwSwapBuffers(this->window);
	glfwPollEvents();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//cleaning frame buffer
}

void Display::setSize(unsigned int width, unsigned int height)
{
	this->screenWidth = width;
	this->screenHeight = height;
	glViewport(0, 0, this->screenWidth, this->screenHeight);
}

GLFWwindow* Display::getWindow()
{
	return this->window;
}

int Display::getWidth()
{
	return this->screenWidth;
}

int Display::getHeight()
{
	return this->screenHeight;
}