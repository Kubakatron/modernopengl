#include "stdafx.h"
#include "cubeRenderer.h"

unsigned int cubeRenderer::vao, cubeRenderer::vbo, cubeRenderer::ebo, cubeRenderer::num_indicies = 0;

void cubeRenderer::init()
{
	float vertices[] = {
		//pos:x, y, z;		texcoords
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	};

	unsigned int indices[] = {
		0, 1, 2, 2, 3, 0,
		4, 5, 6, 6, 7, 4,
		8, 9, 10, 10, 4, 8,
		11, 2, 12, 12, 13, 11,
		10, 14, 5, 5, 4, 10,
		3, 2, 11, 11, 15, 3
	};

	num_indicies = sizeof(indices) / sizeof(indices[0]);

//generate
	glGenVertexArrays(1, &vao);//generates vao (num of vaos to create, reference to vao id)
	glGenBuffers(1, &vbo);//generates buffer (num of buffers to create, reference to buffer id)
	glGenBuffers(1, &ebo);//as above

//bind & send data
	glBindVertexArray(vao);//bind vertex array object to "save" all vertex attrib config
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo);//bind buffer to target
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);//send vertex data to buffer

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);//bind ebo to target
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);//send index data to buffer

//attribs to shader
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);//args([layout (location = 0) in shader] - location of vertex attrib, size of vertex attrib, data type,
																				//if data should be normalised, space between vertex attribs, data offset - requires weird cast)
	glEnableVertexAttribArray(0);//(location of vertex attrib)

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));//tex coords
	glEnableVertexAttribArray(1);

//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);//unbind vbo
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);//apparently shouldn't be done when vao is bound
	glBindVertexArray(0);//unbind vao
}

void cubeRenderer::renderCube()
{
	glBindVertexArray(vao);//bind vao
	glDrawElements(GL_TRIANGLES, num_indicies, GL_UNSIGNED_INT, 0);//args(mode, num of indices, type of indices, offset in ebo)
	glBindVertexArray(0);
}

void cubeRenderer::destroy()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
}