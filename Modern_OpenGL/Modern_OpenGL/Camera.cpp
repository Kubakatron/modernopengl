#include "stdafx.h"
#include "Camera.h"


Camera::Camera()
{
	this->cameraPos = glm::vec3(0.0f, 0.0f, 0.0f);
	this->cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	this->cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	this->fov = 45.0f;
	this->pitch = 0.0f;
	this->yaw = 0.0f;
	this->sensitivity = 0.1f;
	this->firstInput = true;
}

Camera::~Camera()
{
}

glm::mat4 Camera::getProjectionMatrix(float aspectRatio)
{
	return glm::perspective(glm::radians(this->fov), aspectRatio, 0.1f, 100.0f);
}

glm::mat4 Camera::getViewMatrix()
{
	cameraFront.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
	cameraFront.y = sin(glm::radians(pitch));
	cameraFront.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
	cameraFront = glm::normalize(cameraFront);

	return glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}

void Camera::move(Direction dir, float speed)
{
	switch (dir)
	{
	case Forward:
		cameraPos += glm::normalize(glm::vec3(cameraFront.x, 0, cameraFront.z)) * speed;
		break;
	case Backward:
		cameraPos -= glm::normalize(glm::vec3(cameraFront.x, 0, cameraFront.z)) * speed;
		break;
	case Left:
		cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * speed;
		break;
	case Right:
		cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * speed;
		break;
	case Up:
		cameraPos += cameraUp * speed;
		break;
	case Down:
		cameraPos -= cameraUp * speed;
		break;
	}
}

void Camera::mouseMove(float xPos, float yPos)
{
	if (this->firstInput)	//avoids camera jump when cursor is first captured
	{
		this->lastX = xPos;
		this->lastY = yPos;
		this->firstInput = false;
	}

	float xOffset = xPos - this->lastX;
	float yOffset = this->lastY - yPos;	//reversed since OpenGL coords go from bottom to top
	this->lastX = xPos;
	this->lastY = yPos;

	xOffset *= this->sensitivity;
	yOffset *= this->sensitivity;

	this->yaw += xOffset;
	this->pitch += yOffset;

	if (this->pitch > 89)
		this->pitch = 89;
	else if (this->pitch < -89)
		this->pitch = -89;
	this->yaw = fmod(this->yaw, 360);
}

glm::vec3 Camera::getFront()
{
	return this->cameraFront;
}

glm::vec3 Camera::getPosition()
{
	return this->cameraPos;
}

float Camera::getFov()
{
	return this->fov;
}

void Camera::setFov(float fov)
{
	this->fov = fov;
}