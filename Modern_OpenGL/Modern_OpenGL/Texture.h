#pragma once
class Texture
{
public:
	static Texture loadTexture(const string& file);
	~Texture();
	void bind(unsigned int textureUnit);

	static const unsigned int TEXTURE_UNIT_NUM = 16;
private:
	Texture(unsigned int id, int width, int height, int colorChannels);
	
	unsigned int id;
	int width, height, colorChannels;
	static unsigned int boundTexture[TEXTURE_UNIT_NUM];
};
