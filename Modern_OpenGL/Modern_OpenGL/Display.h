#pragma once
class Display
{
public:
	Display(unsigned int width, unsigned int height, const string title);
	~Display();
	bool shouldClose();
	void setClearColor(float r, float g, float b, float a);
	void setWireFrameMode(bool enabled);
	void setCursorCapture(bool enabled);
	void update();
	void Display::setSize(unsigned int width, unsigned int height);
	GLFWwindow* getWindow();
	int getWidth();
	int getHeight();
private:
	GLFWwindow* window;
	int screenWidth, screenHeight;
};
